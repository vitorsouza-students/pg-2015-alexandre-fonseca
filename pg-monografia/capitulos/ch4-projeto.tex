% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 3 - Projeto Arquitetural e Implementação
% ==============================================================================
\chapter{Projeto e Implementação}
\label{sec-projeto}

Apos a identificação, documentação e modelagem dos requisitos, ocorre a fase de projeto, onde o software é modelado de forma que os aspectos tecnológicos que serão utilizados para a implementação do sistema sejam levados em conta. Esses aspectos envolvem: linguagem de programação, bibliotecas e APIs utilizadas, características de interface com o usuário, arquitetura de software e forma de persistência de dados. Por fim, o produto de software é construído e testado.

Neste capítulo, são apresentados os principais aspectos de Projeto do PDEMT.  Na seção~\ref{sec-projeto-arquitetura}, a arquitetura de software é descrita. Na seção~\ref{sec-projeto-apis}, as API's utilizadas no projeto são apresentadas. Na seção~\ref{sec-projeto-componentesArquitetura} é feito o detalhamento dos componentes da arquitetura. Por fim, na seção~\ref{sec-projeto-pdemt}, são apresentadas algumas telas e características da ferramenta. O projeto completo encontra-se nos Apêndices, que também contém informações sobre as tecnologias utilizadas e sobre as táticas de projeto consideradas para atender os requisitos não funcionais identificados no Documento de Requisitos do PDEMT.

\section{Arquitetura de Software}
\label{sec-projeto-arquitetura}
A arquitetura de software do PDEMT esta dividida em camadas, conforme a  Figura~\ref{figura:ArquiteturaPDEMT}. Devido à complexidade do sistema, não foi necessário dividi-lo em subsistemas. A arquitetura é composta por 3 camadas, a saber:
\begin{itemize}
	\item Camada de Interface com o Usuário (CIU), que trata de aspectos relacionados às interfaces gráficas com os usuários; 
	
	\item Camada de Lógica de Negócio (CLN), onde são implementadas as funcionalidades do sistema; e
	
	\item Camada de Gerência de Dados (CGD), responsável pela persistência de objetos.
\end{itemize}

A camada CLN é dividida ainda em duas outras: Camada do Domínio do Problema (CDP) e Camada de Gerência de Tarefas (CGT). A Figura~\ref{figura:ArquiteturaPDEMT} retrata bem isso.

\begin{figure}[!h]
\centering 
\includegraphics[scale=0.6]{figuras/ArquiteturaPDEMT} 
\caption{Arquitetura de Software do PDEMT} 
\label{figura:ArquiteturaPDEMT} 
\end{figure}

Destaca-se na CGT, especificamente falando da classe \texttt{AplDiscurso}, a integração com o Google Calendar, para criação de eventos na Agenda do Google.

\section{API's Utilizadas}
\label{sec-projeto-apis}

Na implementação do PDEMT foram utilizadas duas APIs existentes: a do Google Calendar, provida pela Google, e a API JDBC, que faz parte da distribuição padrão de Java.

\subsection{Google Calendar}

O objetivo da integração com essa API~\cite{googleCalendar} é lembrar/avisar o participante da EMT  de sua designação de discurso. Assim, tendo ele uma conta Google\footnote{\url{https://accounts.google.com/}}---e, por conseguinte, um e-mail no Gmail\footnote{\url{https://mail.google.com/}}---, ele pode ser notificado, até mesmo por meio de um celular do tipo \textit{smartphone} (ex.: celulares Android, iPhone, Windows Phone, etc.), pois este contém (ou pode ser instalado) o aplicativo Calendário integrado ao Google.

Para que o objetivo do projeto seja alcançado, decidiu-se criar uma agenda com o nome ``Calendário do PDEMT'', em uma conta do Google. Assim, todas as designações são inseridas nessa agenda - são os chamados \textbf{Eventos}.

Dentre as propriedades de um Evento, existe uma chamada ``Attendees''. Isso significa que um Evento possui uma lista de participantes. Assim, para que um orador/ajudante da EMT possa ser avisado de seu discurso pela agenda do Google, ele é marcado como um participante desse discurso, por meio do seu endereço de email do Gmail. Assim, o Evento criado em ``Calendário do PDEMT'' é replicado na agenda pessoal do participante.

O primeiro passo para utilização do Google Calendar é a autenticação. Muitos métodos foram encontrados na internet, sendo alguns depreciados e/ou desatualizados. A opção escolhida foi disponibilizada pela Google API Client Library for Java~\cite{clientJava}, como mostram as Listagens~\ref{calendar1} e~\ref{calendar2}:
 
\begin{lstlisting}[caption={Método \textit{authorize}, que faz a leitura do arquivo \textit{client secrets}, usado na autenticação em uma conta do Google}, frame=single, label={calendar1}]  
    public Credential authorize() throws Exception {
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(new FileInputStream(new File("client_secrets.json"))));
        if (clientSecrets.getDetails().getClientId().startsWith("Enter") || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
            System.out.println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=calendar into client_secrets.json");
            System.exit(1);
        }
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets, Collections.singleton(CalendarScopes.CALENDAR)).setDataStoreFactory(dataStoreFactory).build();
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }
\end{lstlisting}

\begin{lstlisting}[caption={Construtor da classe que gerencia o Google Calendar}, frame=single, label={calendar2}]
    public GerenciadorGoogleCalendar() throws GeneralSecurityException, IOException, Exception {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
        credential = authorize();
        client = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
        loadProperties();
    }
\end{lstlisting}

Ao invés de usar login e senha para autenticação, esse método faz a leitura do arquivo \textit{client secrets}, que pode ser obtido\footnote{\url{https://console.developers.google.com/project/primeval-smile-94121/apiui/credential?authuser=0}} no formato \texttt{.json}, após a criação de um projeto no site Google Developer.

Para a criação de eventos, é preciso passar como parâmetro um identificador de calendário, que no caso do PDEMT estará guardado no arquivo de configuração, e o próprio evento, que é populado com as informações das designações.

\subsection{JDBC}
A API Java Database Connectivity (JDBC) permite o desenvolvimento de aplicações conectadas a banco de dados com grande performance. JDBC alcança seu objetivo por meio de um conjunto de interfaces em Java, sendo que este conjunto é implementado diferentemente por cada fabricante. Ao conjunto de classes que implementam o mecanismo de um banco de dados particular se dá o nome de driver JDBC. Para o PDEMT, foi utilizado o Driver do PostgreSQL, o postgresql-9.3-1101. As classes e interfaces mais importantes e utilizadas são : DriverManager, Connection, PreparedStatement e ResultSet. Abaixo segue uma breve descrição da utilização de cada uma delas.

\begin{lstlisting}[caption={Método que cria a conexão com o banco}, frame=single, label={jdbc1}]
    public static Connection getConnection() throws IOException {        
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(getHost(), getLogin(), getPassword());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
\end{lstlisting}

A partir da Listagem~\ref{jdbc1}, observamos o uso da classe \textbf{DriverManager} na linha 3. É nessa linha que o driver que se deseja usar é registrado na classe, para que se possa usar os métodos estáticos para gerenciar o driver JDBC. Após o driver estar inicializado, pode-se abrir uma conexão através do método getConnection, que retorna um objeto \textbf{Connection}, que é o método apresentado na Listagem~\ref{jdbc1}. Foi usada a sobrecarga do método que pede as credenciais de login, senha e host, que estão no arquivo de configuração do PDEMT.

Fazemos uso de \textbf{PreparedStatement} na Listagem~\ref{jdbc2}. Dessa forma é criada uma instrução em SQL que é executada por meio da conexão criada pelo método GetConnection, mostrado na Listagem~\ref{jdbc1}.

\begin{lstlisting}[caption={Inserção de uma cena usando o JDBC}, frame=single, label={jdbc2}]
    public void inserir(Cena entidade) throws SQLException {
        String insert = "INSERT INTO cena(nome) VALUES(?)";
        super.inserir(insert, entidade.getNome());
    }

    protected void inserir(String insertSql, Object... parametros) throws SQLException {
        PreparedStatement pstmt = getConnection().prepareStatement(insertSql);
        for (int i = 0; i < parametros.length; i++) {
            pstmt.setObject(i + 1, parametros[i]);
        }
        pstmt.execute();
        pstmt.close();
        closeConnection();
    }
\end{lstlisting}

Por fim, \textbf{ResultSet} representa o conjunto de resultados de uma tabela do banco de dados. Ele inicia apontando para a primeira linha dos resultados e assim, usando os métodos \textit{getInt, getString, getDouble, getFloat, getLong} e outros, é possível recuperar todos os dados retornados da consulta, conforme mostra a Listagem~\ref{jdbc3}. 

\begin{lstlisting}[caption={Busca de pessoas com parâmetros usando o JDBC}, frame=single, label={jdbc3}]
    public ArrayList<Pessoa> buscarPorSexoAtivas(String sexo) throws SQLException {
        ArrayList<Pessoa> pessoas = new ArrayList<>();
        String select = "SELECT * FROM pessoa WHERE sexo=? and ativo=true";
        PreparedStatement stmt = getConnection().prepareStatement(select);
        stmt.setObject(1, sexo);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Pessoa p = new Pessoa();
            p.setId((int) rs.getLong("id_pessoa"));
            p.setNome(rs.getString("nome"));
            p.setEmail(rs.getString("email"));
            p.setSexo(rs.getString("sexo"));
            p.setAjudante(rs.getBoolean("ajudante"));
            p.setServo(rs.getBoolean("servo"));
            p.setAnciao(rs.getBoolean("anciao"));
            p.setAtivo(rs.getBoolean("ativo"));
            pessoas.add(p);
        }
        rs.close();
        stmt.close();
        return pessoas;
    }
\end{lstlisting}


\subsection{iText-pdf}
O uso dessa API provê a funcionalidade de gerar relatórios no formato PDF. Ela possui diversas características, como: gerar arquivos PDF, gerar formulários em PDF para preenchimento, dividir, fundir, ou sobrepor arquivos PDF, proteger seus arquivos PDF com senha, dentre outras\footnote{\url{http://itextpdf.com/functionality}}. Para criar e manipular os arquivos, a API possui métodos e classes com grande variedade de funcionalidades, tais como criar e editar tabelas e células, bem como todo seu \textit{layout}; criar e editar parágrafos; alterar a fontes e cores e tamanho da página.
Foram criados métodos para cada tipo de relatório a ser gerado pelo PDEMT, e ao gerar esses relatórios, ficam salvos em uma pasta nomeada com o período que abrange os relatórios.

Para a geração dos relatórios, salvo as particularidades de cada um, o procedimento básico foi: criar um documento, definindo seu tamanho (no caso, A4); definir o título do documento; criar uma tabela, criar o cabeçalho da tabela, criar células e preenchê-las com parágrafos; e por fim, adicionar essas células na tabela. As Figuras~\ref{figura:relatorioDirigente},~\ref{figura:relatorioQuadro} e~\ref{figura:relatorioIndividual} mostram exemplos de cada tipo de relatório gerado pelo PDEMT. 

\section{Detalhamento dos Componentes da Arquitetura}
\label{sec-projeto-componentesArquitetura}
O PDEMT está organizado em três camadas: Camada de Lógica de Negócio, Camada de Interface com o Usuário e Camada de Gerência de Dados. A seguir o projeto dessas camadas é apresentado.

\subsection{Camada de Lógica de Negócio (CLN)}
\label{sec-projeto-componentesArquitetura-cln}
Para organizar a Camada de Lógica de Negócio, foi escolhido o padrão Camada de Serviço~\cite{fowler:2002}. Sendo assim, essa camada é dividida em dois componentes: Componente de Domínio do Problema (CDP) e Componente de Gerência de Tarefas (CGT), como apresentado anteriormente na Figura~\ref{figura:ArquiteturaPDEMT}. Esse padrão utiliza um componente para tratar a lógica de aplicação (o CGT), o qual recebe as requisições da interface, e um componente para tratar os conceitos do domínio do problema, advindos do modelo conceitual estrutural elaborado na fase de análise (o CDP).

A Figura~\ref{figura:CDP2} apresenta o diagrama de classes do CDP. Partindo da Figura~\ref{figura:CDP}, as navegabilidades entre as classes foram identificadas, os tipos específicos de domínio foram representados como classes e a visibilidade dos atributos foi explicitada.
\begin{figure}[h]
\centering 
\includegraphics[scale=0.65]{figuras/CDP} 
\caption{Componente de Domínio do Problema - CDP} 
\label{figura:CDP2} 
\end{figure}

No projeto do CGT, optou-se por agrupar os casos de uso de mesmo contexto em uma mesma classe de aplicação, conforme as Figuras~\ref{figura:CGT1} e~\ref{figura:CGT2}. 

\begin{figure}[h]
\centering 
\includegraphics[scale=0.43]{figuras/CGT1} 
\caption{Componente de Gerência de Tarefas dos casos de uso mais básicos e designação de discursos} 
\label{figura:CGT1} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.75]{figuras/CGT2} 
\caption{Componente de Gerência de Tarefas dos casos de uso referentes a relatórios e consultas} 
\label{figura:CGT2} 
\end{figure}

A Tabela~\ref{tabela:CGTeCasosDeUso} sumariza as relações existentes entre as classes do CGT e os casos de uso por elas tratados. É preciso dizer que o caso de uso \textbf{Designar Discurso} é formado pelos métodos \textbf{designarDestaques}, \textbf{designarNumero1}, \textbf{designarNumero2} e \textbf{designarNumero3}.

\begin{table}[!h]
\begin{tabular}{@{}cc@{}}
\toprule
\textbf{Classe}               & \textbf{Casos de Uso}                                                                                                                                      \\ \midrule
AplDiscurso          & \begin{tabular}[c]{@{}c@{}}Cadastrar Discurso, Designar Discurso, Habilitar Google Calendar, \\ Criar Designação na Agenda do Google\end{tabular} \\ \hline
AplCena              & Cadastrar Cena                                                                                                                                    \\  \hline
AplOratória          & Cadastrar Oratória                                                                                                                                \\  \hline
AplPessoa            & Cadastrar Pessoa                                                                                                                                  \\  \hline
AplEmitirRelatórios  & \begin{tabular}[c]{@{}c@{}}Gerar Relatório Dirigente, Gerar Relatório Quadro de Anúncios, \\ Gerar Relatório Individual\end{tabular}              \\  \hline
AplRealizarConsultas & \begin{tabular}[c]{@{}c@{}}Realizar\\ Consulta de Frequência\end{tabular}                                                                         \\ \bottomrule
\end{tabular}
\caption{Mapeamento entre Classes do CGT e Casos de Uso}
\label{tabela:CGTeCasosDeUso}
\end{table}

\subsection{Camada de Interface com o Usuário (CIU)}
Devido a baixa complexidade e o fato de ter mecanismos suficientes para uma boa modularização, não foi necessária a criação de classes controladoras para realizar a interação entre o modelo e a visão do sistema. O padrão Camada de Serviço~\cite{fowler:2002} foi o suficiente para uma boa organização do código. A Figura~\ref{figura:CIU} mostra a relação entre as classes de visão e as classes da CGT, apresentadas na seção~\ref{sec-projeto-componentesArquitetura-cln}.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.55]{figuras/CIU} 
\caption{Arquitetura da CIU} 
\label{figura:CIU} 
\end{figure}

\subsection{Camada de Gerência de Dados (CGD)}

Para organizar e padronizar essa camada, foi adotado o uso de interfaces. As mais comuns e principais operações de um objeto foram declaradas em uma interface genérica chamada \textbf{IGenericDAO}, a qual todas as interfaces exclusivas dos objetos herdam dela, conforme a Figura~\ref{figura:ArquiteturaCGD}.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.5]{figuras/arquiteturaCGD} 
\caption{Arquitetura da CGD} 
\label{figura:ArquiteturaCGD} 
\end{figure}

Para cada classe de domínio a ser persistida, foram criadas uma classe *DAO e uma interface I*DAO correspondente, que implementa a interface genérica IGenericDAO. A primeira deve herdar de GenericDAO, uma classe abstrata que possui as funcionalidades básicas de acesso ao mecanismo de persistência, e deve implementar a interface DAO associada. A Figura~\ref{figura:CGD} mostra o mapeamento do sistema.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.5]{figuras/CGD} 
\caption{A Camada de Gerência de Dados} 
\label{figura:CGD} 
\end{figure}

A Figura \ref{figura:MER}  mostra o modelo Entidade-Relacionamento que foi feito para a implementação das tabelas no banco de dados.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.5]{figuras/MER} 
\caption{Modelo Entidade-Relacionamento} 
\label{figura:MER} 
\end{figure}

\section{O PDEMT}
\label{sec-projeto-pdemt}
Nesta seção, são apresentadas algumas telas da ferramenta PDEMT. A parte cadastral da ferramenta permite o armazenamento de alunos, discursos, cenas e oratórias. A partir desses dados, é possível designar discursos e registrá-los em sua agenda pessoal do Google, gerar relatórios a partir das designações realizadas e fazer consultas que apoiam as escolhas ao designar discursos. A Figura~\ref{figura:principal} mostra a tela principal do sistema.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.4]{figuras/principal} 
\caption{Tela principal do PDEMT} 
\label{figura:principal} 
\end{figure}

A partir da tela principal, o usuário tem acesso a todas as funcionalidades do sistema. As funcionalidades estão distribuidas nos menus de acordo com a entidade, e não de acordo com o tipo de funcionalidade. Por exemplo, temos um menu chamado Alunos, que ao clicar nele são mostrados os itens de menu ``Cadastrar Aluno'' e ``Gerenciar Alunos''. Esse é o padrão para todas as entidades de cadastro. A Figura~\ref{figura:cadastrarAluno} mostra a tela de cadastro de aluno, enquanto a Figura~\ref{figura:gerenciarAlunos} mostra a tela de gerenciamento de alunos.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.55]{figuras/cadastrarAluno} 
\caption{Tela de Cadastro de Aluno} 
\label{figura:cadastrarAluno} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.55]{figuras/gerenciarAlunos} 
\caption{Tela de Gerenciamento de Alunos} 
\label{figura:gerenciarAlunos} 
\end{figure}

O PDEMT possui alguns padrões de tela. Por exemplo, as telas de cadastro são utilizadas exclusivamente para cadastrar entidades no sistema; só fazem a operação de criação. Ja as telas de gerenciamento, fazem a função de consulta ou visualização, edição e exclusão.

Após ter as entidades devidamente cadastradas, podemos usar a principal funcionalidade do PDEMT: a designação de discursos. Podemos selecionar o menu Discursos e em seguida a opção Designar, conforme a Figura~\ref{figura:menuDiscursos}. Após o clique, um \textit{pop-up} é aberto, semelhante à Figura~\ref{figura:designar}. Após a escolha do período que abrangerá as designações, é aberta uma tela contendo os discursos a serem designados, divididos primeiramente por semana, e depois por tipo de discurso, vide Figura~\ref{figura:semanas}.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.55]{figuras/menuDiscursos} 
\caption{Primeiro passo para Designar Discursos - selecionar a opção Designar no menu Discursos} 
\label{figura:menuDiscursos} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.55]{figuras/designar} 
\caption{Segundo passo para Designar Discursos - escolher um período} 
\label{figura:designar} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.37]{figuras/semanas} 
\caption{Terceiro passo para Designar Discursos - escolher oradores, cenas, oratórias e ajudantes para os discursos} 
\label{figura:semanas} 
\end{figure}

Após selecionar todas as opções para todos os discursos de cada semana, o usuário pode clicar no botão Salvar, que esta no painel de cada semana. A ação desse botão garante a persistência dos dados, salvando tudo sobre a designação. Caso a opção Habilitar Google Calendar estiver marcada, como na Figura~\ref{figura:habilitarGoogleCalendar}, o PDEMT também criará eventos na agenda do Google de cada usuário, afim de que eles tenham ciência de suas designações. As Figuras~\ref{figura:agendaSemana} e~\ref{figura:agendaEditarEvento} mostram uma designação recebida na agenda do Google.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/habilitarGoogleCalendar} 
\caption{Opção que permite habilitar ou não o uso da marcação de eventos na agenda do Google} 
\label{figura:habilitarGoogleCalendar} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/agendaSemana} 
\caption{Agenda pessoal de um aluno após a inserção de uma designação} 
\label{figura:agendaSemana} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/agendaEditarEvento} 
\caption{Detalhamento da designação na agenda do Google} 
\label{figura:agendaEditarEvento} 
\end{figure}

Finalmente, as Figuras ~\ref{figura:relatorioDirigente},~\ref{figura:relatorioQuadro} e~\ref{figura:relatorioIndividual}  mostram trechos dos relatórios gerados pelo PDEMT.

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/relatorioDirigente} 
\caption{Trecho de um relatório feito para o dirigente da EMT} 
\label{figura:relatorioDirigente} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/relatorioQuadro} 
\caption{Trecho de um relatório feito para ser fixado no Quadro de Anúncios da Congregação} 
\label{figura:relatorioQuadro} 
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[scale=0.6]{figuras/relatorioIndividual} 
\caption{Exemplo de 2 relatórios individuais, a serem entregues aos participantes que farão os discursos} 
\label{figura:relatorioIndividual} 
\end{figure}